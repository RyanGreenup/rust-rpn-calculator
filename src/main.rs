use std::{env, process::exit};
mod rpn_calculator;
pub use crate::rpn_calculator::rpn_calculator;

fn main() {
    let args: Vec<String> = env::args().collect();
    test_args(&args);
    let stack = rpn_calculator(args[1].trim());

    for val in stack {
        println!("{}", val);
    }
}

fn test_args(args: &Vec<String>) {

    if args.len() > 2 {
        help(&args);
        exit(1);
    } else if args.contains(&"help".to_string())
        || args.contains(&"-h".to_string())
        || args.contains(&"--help".to_string())
        || args.contains(&"-help".to_string())
    {
        help(&args);
        exit(0);
    }
}

fn help(args: &Vec<String>) {
    println!(
        r###"
    Usage
    ------------------
    {} [String]

    Example
    ------------------
    {} '3 2 *'

    Shortcuts
    ------------------
    u:\t Undo
    c:\t Copy
    s:\t Swap
    i:\t Inverse (1/x)

    Commands
    ------------------
    sin/cos/tan <==> asin/acos/atan
    sinh/cosh/tanh
    ! (factorial)
    
    
    "###,
        args[0], args[0]
    );
}
