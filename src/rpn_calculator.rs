use std::{process::exit};

const DEV: bool = true;
const PI: f64 = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;
const E: f64 = 2.71828182845904523536028747135266249775724709369995;
const PHI: f64 = 1.618;

pub fn rpn_calculator(operation: &str) -> Vec<f64> {
    let mut stack: Vec<f64> = Vec::new();

    let operation = operation.trim();
    for s in operation.split(' ') {
        match s.trim() {
            // binary
            "+" => binary_operator(&mut stack, |a, b| a + b),
            "*" => binary_operator(&mut stack, |a, b| a * b),
            "^" => binary_operator(&mut stack, |a, b| a.powf(b)),
            // minus is nice as a toggle
            "-" => {
                if stack.len() == 1 {
                    unary_operator(&mut stack, |a| -a)
                } else {
                    binary_operator(&mut stack, |a, b| a - b)
                }
            }
            "/" => binary_operator(&mut stack, |a, b| a / b),
            "%" => binary_operator(&mut stack, |a, b| a % b),
            "log" => binary_operator(&mut stack, |a, b| a.log(b)),
            // Unary
            "log2" => unary_operator(&mut stack, |a| a.log2()),
            "log10" => unary_operator(&mut stack, |a| a.log10()),
            "log" => unary_operator(&mut stack, |a| a.log(E)),
            "sin" => unary_operator(&mut stack, |a| a.sin()),
            "asin" => unary_operator(&mut stack, |a| a.asin()),
            "sinh" => unary_operator(&mut stack, |a| a.sinh()),
            "round" => unary_operator(&mut stack, |a| a.round()),
            "cos" => unary_operator(&mut stack, |a| a.cos()),
            "acos" => unary_operator(&mut stack, |a| a.acos()),
            "cosh" => unary_operator(&mut stack, |a| a.cos()),
            "tan" => unary_operator(&mut stack, |a| a.tan()),
            "atan" => unary_operator(&mut stack, |a| a.atan()),
            "tanh" => unary_operator(&mut stack, |a| a.tanh()),
            "sq" => unary_operator(&mut stack, |a| a.powf(2.0)),
            "sqrt" => unary_operator(&mut stack, |a| a.sqrt()),
            // Combinatorics
            "!" => unary_operator(&mut stack, |a| (factorial(a as i64) as f64)),
            "choose" => binary_operator(&mut stack, |a, b| choose(a as i64, b as i64) as f64),
            // Manipulation
            "s" => swap(&mut stack),
            "c" => copy(&mut stack),
            "u" => undo(&mut stack),
            "i" => inverse(&mut stack),
            // Otherwise assume it's a number
            _ => add_to_stack(&mut stack, s),
        }
    }
    return stack;
}


fn inverse(stack: &mut Vec<f64>) {
    let n = stack.len();
    let a = stack[n-1];
    stack[n-1] = 1./a;
}


fn undo(stack: &mut Vec<f64>) {
    stack.pop();
}

fn copy(stack: &mut Vec<f64>) {
    let n = stack.len();
    let a = stack[n-1];
    stack.push(a);
}


fn swap(stack: &mut Vec<f64>) {
    let n = stack.len();
    let a = stack[n-1];
    let b = stack[n-2];
    stack[n-1] = b;
    stack[n-2] = a;
}

fn choose(n: i64, r: i64) -> i64 {
    return factorial(n) / (factorial(r) * factorial(n - r));
}

fn factorial(mut n: i64) -> i64 {
    if n > 20 {
        println!("Can only take factorial of integers < 20  ∵ 𝐙₆₄");
        return n;
    } else {
        let mut out = 1;
        while n > 1 {
            out = out * n;
            n = n - 1
        }
        return out;
    }
}


fn add_to_stack(stack: &mut Vec<f64>, num: &str) {
    // TODO add match for pi and e

    match num.trim() {
        "pi" => {
            stack.push(PI);
            return;
        }
        "e" => {
            stack.push(E);
            return;
        }
        "phi" => {
            stack.push(PHI);
            return;
        }
        _ => match num.trim().parse() {
            Ok(fnum) => stack.push(fnum),
            Err(err) => {
                if DEV {
                    println!("Dropped a bad argument: \n --> {}", num);
                    println!("{}", err)
                }
            }
        },
    }
}

fn binary_operator(stack: &mut Vec<f64>, func: fn(f64, f64) -> f64) {
    if stack.len() > 1 {
        let a = match stack.pop() {
            Some(num) => num,
            None => {
                println! {"No value to pop"};
                exit(1);
            } // TODO Error Handling
        };

        let n = stack.len();
        // TODO make this a pointer??
        stack[n - 1] = func(stack[n - 1], a)
    }
}

fn unary_operator(stack: &mut Vec<f64>, func: fn(f64) -> f64) {
    if stack.len() > 0 {
        let n = stack.len();
        stack[n - 1] = func(stack[n - 1])
    }
}
